module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}", ],
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'g1': '#E0DBD0',
        'g2': '#F6F4EF',
        'black/50': 'rgba(0, 0, 0, 0.50)',
        'yesIcon': '#8DCC7B;',
        'noIcon': '#FF5B5B;',
        
      },
      backgroundImage: {
        'BigG': "url('./assets/Big-G.svg')",
      },
      height: {
        '117': '18.72rem',
      },
      flex: {
        'md': '0 0 32%',
        'sm': '0 0 100%'
      },
      maxWidth: {
        '1180': '1180px',
      },
      dropShadow: {
        'my-sm': '-4px 4px 4px rgba(0, 0, 0, 0.2)',
        'my-md': '-4px 4px 4px rgba(0, 0, 0, 0.2)'
      },
      borderRadius: {
        'my-sm': ' 8px',
        'my-md': '5.82996px'
       
      },
      borderWidth: {
        '05': '0.5px',
      },
      margin: {
        'minus-1px': '-1px',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
