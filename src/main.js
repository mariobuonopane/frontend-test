import {createApp} from 'vue'
import * as VueRouter  from 'vue-router'
import Person from "./components/Person.vue";
import Index from "./components/Index.vue";

import App from './App.vue'
import './index.css'


const routes = [
    { path: '/', component: Index },
    { path: '/person/:id', name:"person", component: Person},
];

const router = VueRouter.createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: VueRouter.createWebHashHistory(),
    routes, // short for `routes: routes`
});

const app = createApp(App);
app.use(router);

app.mount('#app');

