import axios from 'axios'

const list = async () => {
    try {
        console.log(process.env.VUE_APP_LIST_PEOPLE_URL);

        const config = {
            params: {api_key: process.env.VUE_APP_API_TOKEN}
        };

        console.log("config:" + JSON.stringify(config));

        let data = await axios.get(process.env.VUE_APP_PEOPLE_URL, config);

        //        console.log("read data:" + JSON.stringify(data));

        let recs = data.data.records;

        var res = recs.map(convertPersonFromRest);

        return res;
    } catch (error) {
        console.error(error);
        return undefined;
    }

}

function convertPersonFromRest (person) {
    let fields = person?.fields;
    return {
        linkedInUrl: fields['Linkedin url'],
        twitterUrl: fields['Twitter url'],
        largeImg: fields['Profile image'][0]?.thumbnails?.large.url,
        fullImg: fields['Profile image'][0]?.thumbnails?.full.url,
        name: fields?.Name,
        role: fields?.Role,
        quote: fields.Quote,
        bio: fields.Bio,
        id: person.id,
        requestable: fields['Is requestable']
    }
}

const personDetail = async (personId) => {
    try {
        //console.log(process.env.VUE_APP_LIST_PEOPLE_URL);

        const config = {
            params: {api_key: process.env.VUE_APP_API_TOKEN}
        };

        let data = await axios.get(process.env.VUE_APP_PEOPLE_URL+"/"+personId, config);

        //        console.log("read data:" + JSON.stringify(data));

        let res = convertPersonFromRest(data.data);

        return res;
    } catch (error) {
        console.error(error);
        return undefined;
    }

}



const friendRequest = async (from, toPersonId) => {

    try {
        const headers = {
            Authorization: 'Bearer ' + process.env.VUE_APP_API_TOKEN,
            'Content-Type': 'application/json'
        }

        let body = {
            "records": [
                {
                    "fields": {
                        "From": from,
                        "To": [
                            toPersonId
                        ]
                    }
                }
            ]
        }
        console.log("process.env.VUE_APP_FRIEND_REQUEST_URL:" + process.env.VUE_APP_FRIEND_REQUEST_URL);
        console.log("headers:" + JSON.stringify(headers));
        console.log("body:" + JSON.stringify(body));

        let res = await axios.post(process.env.VUE_APP_FRIEND_REQUEST_URL, body, {
            headers: headers
        })

        console.log("res:" + JSON.stringify(res));

        return true;

    } catch (error) {
        console.error(error);
        return false;
    }

}


export {
    list,
    friendRequest,
    personDetail
}